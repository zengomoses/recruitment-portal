from django.conf.urls import url
from django.urls import path, include, re_path

from main.views import HomeView, AboutView, ContactView, JobListView, JobDetailView, JobsSearchView, JobThroughWithCategorySelectListView

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('about', AboutView.as_view(), name="about"),
    path('contact', ContactView.as_view(), name="contact"),
    path('jobs', JobListView.as_view(), name="jobs"),
    path('jobs/<int:pk>', JobDetailView.as_view(), name='job-detail'),
    re_path(r'^jobs/search$', JobsSearchView.as_view(), name="job-search"),
    path('jobs/category/<int:pk>',  JobThroughWithCategorySelectListView.as_view(), name='job-category'),
    url(r'^updates/', include('newsletters.urls')),

]