from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.db.models import Count, Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView, FormView
from django_filters.rest_framework import DjangoFilterBackend
from accounts.models import User
from configuration.models import JobCategory
from job.models import Post
from newsletters.forms import NewsletterUserSignUpForm
from newsletters.models import NewsletterUser


class HomeView(FormView):
    template_name = 'main/index.html'
    form_class = NewsletterUserSignUpForm
    success_url = '/'

    def get_form(self, **kwargs):
        form = self.form_class

        return form(self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(HomeView, self).get_form_kwargs()
        return kwargs

    def post(self, request, *args, **kwargs):
        form = NewsletterUserSignUpForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            if NewsletterUser.objects.filter(email=instance.email).exists():
                messages.warning(request, 'Email exits in records',
                                 'alert alert-warning fade in alert-dismissible show'
                                 )
            else:
                instance.save()
                messages.success(request, 'Thank you for subscribing..!',
                                 'alert alert-success fade in alert-dismissible show'
                                 )
                subject = "Thanks for subscribing to Recruitment Portal Updates"
                from_email = settings.EMAIL_HOST_USER
                to_email = [instance.email]
                signup_message = """Welcome to Recruitment Portal Updates, you will be the first to receive some information updates wherever they occur,
                                    To unsubscribe visit this link http//127.0.0.1:8000/account/newsletter/unsubscribe
                                    """
                send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=signup_message, fail_silently=False)

        context = {
            'form': form
        }
        return render(request, 'main/index.html', context)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['job_count'] = Post.objects.all().count()
        context['categories'] = JobCategory.objects.all().annotate(job_posts=Count('category_post'))
        context['latest_jobs'] = Post.objects.filter(published_on__isnull=False).order_by('-published_on')[:6]

        return context


class AboutView(FormView):
    template_name = 'main/about.html'
    form_class = NewsletterUserSignUpForm
    success_url = '/'

    def get_form(self, **kwargs):
        form = self.form_class

        return form(self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(AboutView, self).get_form_kwargs()
        return kwargs

    def post(self, request, *args, **kwargs):
        form = NewsletterUserSignUpForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            if NewsletterUser.objects.filter(email=instance.email).exists():
                messages.warning(request, 'Email exits in records',
                                 'alert alert-warning fade in alert-dismissible show'
                                 )
            else:
                instance.save()
                messages.success(request, 'Thank you for subscribing..!',
                                 'alert alert-success fade in alert-dismissible show'
                                 )
                subject = "Thanks for subscribing to Recruitment Portal Updates"
                from_email = settings.EMAIL_HOST_USER
                to_email = [instance.email]
                signup_message = """Welcome to Recruitment Portal Updates, you will be the first to receive some information updates wherever they occur,
                                       To unsubscribe visit this link www.careerconnect.co.tz/updates/unsubscribe
                                       """
                send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=signup_message,
                          fail_silently=False)

        context = {
            'form': form
        }
        return render(request, 'main/about.html', context)

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)

        return context


class ContactView(TemplateView):
    template_name = 'main/contact.html'

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)

        return context


class JobListView(TemplateView):
    template_name = 'main/job-list.html'

    def get_context_data(self, **kwargs):
        context = super(JobListView, self).get_context_data(**kwargs)
        context['jobs'] = Post.objects.all()

        return context


class JobDetailView(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'main/job-detail.html'


class JobThroughWithCategorySelectListView(ListView):
    template_name = 'main/job-list-bycategory.html'
    context_object_name = 'jobs'

    def get_queryset(self):
        request = self.request
        query_id = self.kwargs['pk']

        if query_id is not None:
            queryset = Post.objects.filter(category_id__exact=query_id)
            return queryset


class JobsSearchView(ListView):
    template_name = 'main/job-list.html'
    context_object_name = 'jobs'


    def get_queryset(self):
        request = self.request
        query_title = request.GET.get('title')
        query_location = request.GET.get('location')

        if query_title is not None and query_location is not None:
            queryset = Post.objects.filter(title__icontains=query_title, location__region__icontains=query_location)
            print(queryset)
            return queryset

        elif query_title is None and query_location is not None:
            queryset = Post.objects.filter(location__region__icontains=query_location)
            print(queryset)
            return queryset

        elif query_title is not None and query_location is None:
            queryset = Post.objects.filter(title__icontains=query_title)
            print(queryset)
            return queryset
