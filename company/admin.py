from django.contrib import admin

# Register your models here.
from company.models import Industry, Company


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(Company, CompanyAdmin)
