from django.db import models

from accounts.models import User
from configuration.models import Industry


class Company(models.Model):
    account = models.ForeignKey(User, related_name="company_user", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    industry = models.ManyToManyField(Industry)
    physical_address = models.TextField()
    tel = models.CharField(max_length=25)
    tin_number = models.CharField(max_length=25)
    description = models.TextField()

    class Meta:
        verbose_name = 'Company List'
        verbose_name_plural = 'Company List'

    def __str__(self):
        return self.name

