
from django.urls import path, include

urlpatterns = [
    path('account/', include('accounts.api.urls')),
    path('job/', include('job.api.urls')),
    path('configuration/', include('configuration.api.urls')),
    path('candidate/', include('candidate.api.urls')),
]