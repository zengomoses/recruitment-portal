from allauth.account import app_settings
from allauth.account.utils import get_next_redirect_url, complete_signup, passthrough_next_redirect_url
from allauth.account.views import SignupView, AjaxCapableProcessFormViewMixin, RedirectAuthenticatedUserMixin, \
    CloseableSignupMixin, sensitive_post_parameters_m
from allauth.exceptions import ImmediateHttpResponse
from allauth.utils import get_form_class, get_request_param
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView

from accounts.forms import CustomSignupForm
from backend import settings


class CustomSignupView(RedirectAuthenticatedUserMixin, CloseableSignupMixin,
                       AjaxCapableProcessFormViewMixin, FormView):
    template_name = "account/signup.html"
    form_class = CustomSignupForm
    redirect_field_name = "next"
    success_url = None

    def form_valid(self, form):
        # By assigning the User to a property on the view, we allow subclasses
        # of SignupView to access the newly created User instance
        self.user = form.save(self.request)
        try:
            return complete_signup(
                self.request, self.user,
                settings.ACCOUNT_EMAIL_VERIFICATION,
                self.get_success_url())
        except ImmediateHttpResponse as e:
            return e.response


signup = SignupView.as_view()


