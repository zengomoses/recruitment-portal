from django.test import TestCase

# Create your tests here.
from accounts.models import User


class AccountTests(TestCase):
    """Test module for Accounts modes"""
    def setUp(self):
        User.objects.create(
            first_name='Moses',
            last_name='Zengo',
            date_of_birth='1992-07-16',
            username= 'moses',
            password='admin1234',
            title='Mr.',
            gender='M',
            email='moizes92@gmail.com',
            nationality='Tanzanian',
        )

    def test_get_username(self):
        user = User.objects.get(first_name='Moses')
        self.assertEqual(user.get_username(), "moses")

    def test_get_full_name(self):
        user = User.objects.get(first_name='Moses')
        self.assertEqual(user.get_full_name(), "Moses Zengo")

    def test_check_active_user(self):
        user = User.objects.get(username="moses")
        self.assertEqual(user.check_active_status(), True)
