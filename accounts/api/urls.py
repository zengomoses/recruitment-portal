from allauth.account.views import ConfirmEmailView
from django.urls import path, include, re_path
from rest_framework_jwt.views import refresh_jwt_token

from accounts.api import views

urlpatterns = [
    path('registration/account-email-verification-sent/', views.null_view, name="account_email_verification_sent"),
    re_path(r'^registration/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(), name="account_confirm_email"),
    path('registration/complete/', views.complete_view, name="account_confirm_complete"),
    re_path(r'^password-reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.null_view, name='password_reset_confirm'),
    path('', include('rest_auth.urls')),
    path('registration/', include('rest_auth.registration.urls')),
    path('refresh-token/', refresh_jwt_token),
    path('user/profile/', include('candidate.api.urls'))
]