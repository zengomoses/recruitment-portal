from django.urls import path, include, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase

from accounts.models import User


class AccountAPITests(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('api/', include('urls')),
    ]

    def test_create_account(self):
        """Ensure we can create account object"""
        url = reverse('account-list')
        data = {
            'first_name': 'Moses',
            'last_name': 'Zengo',
            'date_of_birth': '1992-07-16',
            'username': 'moses',
            'password': 'admin1234',
            'title': 'Mr.',
            'gender': 'M',
            'email': 'moizes92@gmail.com',
            'nationality': 'Tanzanian',}
        count = User.objects.count
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count, count + 1)
