from rest_auth.registration.views import RegisterView
from rest_auth.views import UserDetailsView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from accounts.models import User


class UserRegisterView(RegisterView):
    queryset = User.objects.all()


@api_view()
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view()
def complete_view(request):
    return Response("Your account has been activated")
