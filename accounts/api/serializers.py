from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers

from accounts.models import User


class UserRegisterSerializer(RegisterSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField()
    password1 = serializers.CharField(write_only=True)

    def get_cleaned_data(self):
        super(UserRegisterSerializer, self).get_cleaned_data()

        return {
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'email': self.validated_data.get('email', ''),
            'password1': self.validated_data.get('password1', '')
        }


class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('title', 'first_name', 'last_name', 'gender', 'date_of_birth',
                  'phone_number','location', 'avatar', 'email', 'profile_complete', 'step' )
        read_only_fields = ('email',)
