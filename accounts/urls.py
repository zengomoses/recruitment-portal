from django.conf.urls import url
from django.urls import path, include

# from accounts.views import SignUpView, SignInView
from accounts import views

urlpatterns = [
    # path('sign-up/', SignUpView.as_view(), name="signup"),
    # path('log-in/', SignInView.as_view(), name="login"),
    url(r"^register/$", views.signup, name="account_register"),
    path('', include('allauth.urls')),

]