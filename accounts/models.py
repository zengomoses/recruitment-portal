from django.contrib.auth.models import AbstractUser
from django.db import models

from configuration.models import Location


class User(AbstractUser):
    MALE = 'M'
    FEMALE = 'F'
    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    JOB_SEEKER = 'Jobseeker'
    RECRUITER = 'Recruiter'

    ACCOUNT_TYPE = (
        (JOB_SEEKER, 'Jobseeker'),
        (RECRUITER, 'Recruiter')
    )

    DEFAULT_IMAGE = 'avatars/default/placeholder-male.jpg'

    # title = models.CharField(max_length=3, choices=TITLE, null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER, null=True, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    email = models.EmailField(unique=True)
    phone_number = models.CharField(max_length=25, null=True, blank=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, null=True, blank=True)
    avatar = models.ImageField(upload_to='avatars/', default=DEFAULT_IMAGE, null=True, blank=True)
    account_type = models.CharField(max_length=25, choices=ACCOUNT_TYPE, null=True, blank=True)
    profile_complete = models.BooleanField(default=False)
    step = models.IntegerField(default=1)

    class Meta:
        verbose_name = 'User Account'
        verbose_name_plural = 'User Accounts'

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def is_candidate(self):
        if self.account_type == 'Jobseeker':
            return True

    def is_recruiter(self):
        if self.account_type == 'Recruiter':
            return True
