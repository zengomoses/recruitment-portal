import datetime

from allauth.account.adapter import DefaultAccountAdapter
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string
from backend import settings
from configuration.models import Location


class CustomDefaultAccountAdapter(DefaultAccountAdapter):

    def render_mail(self, template_prefix, email, context):
        """
        Renders an e-mail to `email`.  `template_prefix` identifies the
        e-mail that is to be sent, e.g. "account/email/email_confirmation"
        """
        subject = render_to_string('{0}_subject.txt'.format(template_prefix),
                                   context)
        # remove superfluous line breaks
        subject = " ".join(subject.splitlines()).strip()
        subject = self.format_email_subject(subject)

        from_email = self.get_from_email()

        bodies = {}
        for ext in ['html', 'txt']:
            try:
                template_name = '{0}_message.{1}'.format(template_prefix, ext)
                bodies[ext] = render_to_string(template_name,
                                               context).strip()
            except TemplateDoesNotExist:
                if ext == 'txt' and not bodies:
                    # We need at least one body
                    raise
        if 'txt' in bodies:
            msg = EmailMultiAlternatives(subject,
                                         bodies['txt'],
                                         from_email,
                                         [email])
            if 'html' in bodies:
                msg.attach_alternative(bodies['html'], 'text/html')
        else:
            msg = EmailMessage(subject,
                               bodies['html'],
                               from_email,
                               [email])
            msg.content_subtype = 'html'  # Main content is now text/html
        return msg

    def send_mail(self, template_prefix, email, context):
        context['activate_url'] = settings.URL_FRONT + 'account/verify-email/' + context['key']
        msg = self.render_mail(template_prefix, email, context)
        msg.send()

    def save_user(self, request, user, form, commit=True):
        """
        Saves a new `User` instance using information provided in the
        signup form.
        """

        data = form.cleaned_data
        print("FORM DATA: {}".format(data))
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        gender = data.get('gender')
        date_of_birth = data.get('date_of_birth')
        phone_number = data.get('phone_number')
        location = data.get('location')
        account_type = data.get('account_type')
        email = data.get('email')
        username = data.get('username')
        from allauth.account.utils import user_email, user_username, user_field
        user_email(user, email)
        user_username(user, username)
        if first_name:
            user_field(user, 'first_name', first_name)
        if last_name:
            user_field(user, 'last_name', last_name)
        if gender:
            user_field(user, 'gender', gender)
        if date_of_birth:
            date_of_birth = str(date_of_birth)
            user_field(user, 'date_of_birth', date_of_birth)
        if phone_number:
            user_field(user, 'phone_number', phone_number)
        if location:
            user.location = location
        if account_type:
            user_field(user, 'account_type', account_type)
        if 'password1' in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user
