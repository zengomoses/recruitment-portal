from datetime import datetime

from allauth import app_settings
from allauth.account.adapter import get_adapter
from allauth.account.forms import LoginForm, SignupForm, _base_signup_form_class, BaseSignupForm, \
    PasswordVerificationMixin, UserForm, SetPasswordField
from allauth.account.utils import setup_user_email
from allauth.utils import set_form_field_order
from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _

from backend import settings
from configuration.models import Location


class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        render_value = kwargs.pop('render_value',
                                  settings.ACCOUNT_PASSWORD_INPUT_RENDER_VALUE)
        kwargs['widget'] = forms.PasswordInput(render_value=render_value,
                                               attrs={'placeholder':
                                                          kwargs.get("label"),
                                                      'class': 'form-control'
                                                      })
        super(PasswordField, self).__init__(*args, **kwargs)


class CustomLoginForm(LoginForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CustomLoginForm, self).__init__(*args, **kwargs)

        login_widget = forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': _('E-mail'),
            'autofocus': 'autofocus'}
        )
        password_widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'placeholder': _('Password')}
        )

        login_field = forms.CharField(widget=login_widget)
        password_field = forms.CharField(widget=password_widget)

        self.fields["login"] = login_field
        self.fields["password"] = password_field
        set_form_field_order(self, ["login", "password", "remember"])


class CustomBaseSignupForm(BaseSignupForm):

    MALE = 'M'
    FEMALE = 'F'
    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    JOB_SEEKER = 'Jobseeker'
    RECRUITER = 'Recruiter'

    ACCOUNT_TYPE = (
        (JOB_SEEKER, 'Jobseeker'),
        (RECRUITER, 'Recruiter')
    )

    current_year = datetime.now().year

    first_name = forms.CharField(label=_("First Name"),
                                 max_length=200,
                                 widget=forms.TextInput(
                                     attrs={'placeholder':
                                                _('First Name'),
                                            'autofocus': 'autofocus',
                                            'class': 'form-control'}))
    last_name = forms.CharField(label=_("Last Name"),
                                max_length=200,
                                widget=forms.TextInput(
                                    attrs={'placeholder':
                                               _('Last Name'),
                                           'autofocus': 'autofocus',
                                           'class': 'form-control'}))
    gender = forms.ChoiceField(label=_("Gender"), choices=GENDER,
                               widget=forms.Select(
                                   attrs={'placeholder':
                                              _('Gender'),
                                          'autofocus': 'autofocus',
                                          'class': 'form-control'}))
    date_of_birth = forms.DateField(label=_("Date of Birth"),

                                    widget=forms.SelectDateWidget(
                                        years=range(1960, current_year),
                                        attrs={'placeholder':
                                                   _('Birth Date'),
                                               'autofocus': 'autofocus',
                                               'class': 'form-control'}),)

    username = forms.CharField(label=_("Username"),
                               min_length=settings.ACCOUNT_USERNAME_MIN_LENGTH,
                               widget=forms.TextInput(
                                   attrs={'placeholder':
                                              _('Username'),
                                          'autofocus': 'autofocus',
                                          'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(
        attrs={'type': 'email',
               'placeholder': _('E-mail address'),
               'class': 'form-control'}))
    phone_number = forms.CharField(label=_("Mobile Number"),
                                   max_length=200,
                                   widget=forms.TextInput(
                                       attrs={'placeholder':
                                                  _('Mobile Number'),
                                              'autofocus': 'autofocus',
                                              'class': 'form-control'}))
    location = forms.ModelChoiceField(label=_("Your current location"),
                                      queryset=Location.objects.all(),
                                      widget=forms.Select(
                                          attrs={'placeholder':
                                                     _('Location'),
                                                 'autofocus': 'autofocus',
                                                 'class': 'form-control'}))

    account_type = forms.ChoiceField(label=_("Select account type"), choices=ACCOUNT_TYPE,
                                     widget=forms.Select(
                                         attrs={'placeholder':
                                                    _('account type'),
                                                'autofocus': 'autofocus',
                                                'class': 'form-control'}))


class CustomSignupForm(CustomBaseSignupForm):

    def __init__(self, *args, **kwargs):
        super(CustomSignupForm, self).__init__(*args, **kwargs)
        self.fields['password1'] = PasswordField(label=_("Password"))

    def save(self, request):
        adapter = get_adapter(request)
        user = adapter.new_user(request)
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        # TODO: Move into adapter `save_user` ?
        setup_user_email(request, user, [])
        return user


class SetPasswordForm(PasswordVerificationMixin, UserForm):
    password1 = SetPasswordField(label=_("Password"), widget=forms.TextInput(
        attrs={'placeholder':
                   _('Password'),
               'class': 'form-control'}))
    password2 = PasswordField(label=_("Password (again)"))

    def __init__(self, *args, **kwargs):
        super(SetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['password1'].user = self.user

    def save(self):
        get_adapter().set_password(self.user, self.cleaned_data["password1"])
