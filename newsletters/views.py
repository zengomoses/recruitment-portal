from django.conf import settings
from django.contrib import messages
from django.shortcuts import render
from django.core.mail import send_mail
from .models import NewsletterUser
from .forms import NewsletterUserSignUpForm


def newsletter_signup(request):
    form = NewsletterUserSignUpForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        if NewsletterUser.objects.filter(email=instance.email).exists():
            messages.warning(request, 'Email doesnt exits in records',
                             'alert alert-warning fade in alert-dismissible show'
                             )
        else:
            instance.save()
            messages.success(request, 'Thank you for subscribing..!',
                             'alert alert-success fade in alert-dismissible show'
                             )
            subject = "Thanks for subscribing to Recruitment Portal Updates"
            from_email = settings.EMAIL_HOST_USER
            to_email = [instance.email]
            signup_message = """Welcome to Recruitment Portal Updates, you will be the first to receive some information updates wherever they occur,
                                To unsubscribe visit this link http://51.79.53.170/updates/unsubscribe
                                """
            send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=signup_message, fail_silently=False)

    context = {
        "form": form,
    }
    template = "main/about.html"

    return render(request, template, context)


def newsletter_unsubscribe(request):
    form = NewsletterUserSignUpForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        if NewsletterUser.objects.filter(email=instance.email).exists():
            NewsletterUser.objects.filter(email=instance.email).delete()
            messages.success(request, 'Sad to see yo go!!',
                             'alert alert-success fade in alert-dismissible show'
                             )
            subject = "Unsubscribed from Recruitment Portal Updates"
            from_email = settings.EMAIL_HOST_USER
            to_email = [instance.email]
            signup_message = """Sorry to see you go! Let us know if there is an issue with our service
                                            """
            send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=signup_message,
                      fail_silently=False)
        else:
            messages.warning(request, 'Email doesnt exist in records.!',
                             'alert alert-warning fade in alert-dismissible show'
                             )
    context = {
        "form": form,
     }

    template = "main/index_o.html"

    return render(request, template, context)
