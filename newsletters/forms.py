from django import forms
from .models import NewsletterUser

class NewsletterUserSignUpForm(forms.ModelForm):
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'placeholder': 'Email'}),
    )
    class Meta:
        model =NewsletterUser
        fields = ['email']
        def clean_email(self):
            email =self.cleaned_data.get('email')
            return email