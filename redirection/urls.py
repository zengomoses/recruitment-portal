from django.conf.urls import url
from django.urls import path, include

from redirection.views import RedView, CompanyTestView, CompanyCreateView, CompanyCreateStepView

urlpatterns = [
    path('', RedView.as_view(), name='redirect_fx'),
    path('company_test', CompanyTestView.as_view(), name='company_test'),
    path('register-company', CompanyCreateStepView.as_view(), name='company_register'),
    path('companycreate', CompanyCreateView.as_view(), name='company_create'),

]
