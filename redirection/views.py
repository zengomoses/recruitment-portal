from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import CreateView
from django.views.generic.base import View, TemplateView

# def RedirectFx(request):
#     if request.user.is_candidate:
#         return HttpResponseRedirect('/jobseeker/')
#     else:
#         return HttpResponseRedirect('/recruiter/')
from company.models import Company
from configuration.models import Industry


class RedView(TemplateView):
    template_name = 'tester/tester.html'


class CompanyTestView(View):
    template_name = 'tester/tester.html'

    def get(self, request):
        if self.request.user.is_recruiter():
            c = Company.objects.filter(account_id=self.request.user.id)
            print(c)
            if c:
                link = HttpResponseRedirect('/recruiter/')
            else:
                link = HttpResponseRedirect('register-company')

            return link


class CompanyCreateStepView(TemplateView):
    template_name = 'recruiter/company_register.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyCreateStepView, self).get_context_data(**kwargs)
        context['industry'] = Industry.objects.all()

        return context


class CompanyCreateView(View):
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            com = Company()
            print(com)
            # print(Company.objects.all())

            com.account = self.request.user
            com.physical_address = request.POST.get('physical_address')
            com.tin_number = request.POST.get('tin_number')
            com.tel = request.POST.get('tel')
            com.name = request.POST.get('name')
            com.description = request.POST.get('description')
            com.save()

            com.industry.add(Industry.objects.get(id=request.POST.get('industry')))

            response = {'type': 'success', 'message': 'Registration was successfully'}

            return JsonResponse(response)
