from django.urls import path, re_path

from job.api.views import JobListView, JobCreateView, JobCountView, JobsSearchView, JobDetailView, \
    JobApplicationCreateView, JobsMatchedView, SavedJobsListView, SavedJobsCreateView, JobApplicationListView

urlpatterns = [
    path('', JobListView.as_view(), name='jobs-list'),
    path('create', JobCreateView.as_view()),
    path('count', JobCountView.as_view()),
    path('detail/<int:pk>', JobDetailView.as_view()),
    re_path(r'^search$', JobsSearchView.as_view()),
    path('apply', JobApplicationCreateView.as_view()),
    path('applied', JobApplicationListView.as_view()),
    path('matched', JobsMatchedView.as_view()),
    path('save', SavedJobsCreateView.as_view()),
    path('saved', SavedJobsListView.as_view()),
]