from django.db.models import Count
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, ListCreateAPIView
from rest_framework.permissions import *
from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _
from accounts.models import User
from candidate.models import Candidate, Experience, Skill
from job.api.serializers import JobListSerializer, JobDetailSerializer, JobApplicationCreateSerializer, \
    SavedJobListSerializer, SavedJobCreateSerializer, JobApplicationListSerializer
from job.models import Post, Application, SavedJob


class JobCountView(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = JobListSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def list(self, request, *args, **kwargs):
        count = self.get_queryset().count()
        return Response(count)


class JobListView(ListAPIView):
    queryset = Post.objects.filter(published_on__isnull=False).order_by('-published_on')[:6]
    serializer_class = JobListSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class JobCreateView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = JobListSerializer
    permission_classes = [IsAuthenticated]


class JobsSearchView(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = JobListSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ('category', 'type', 'location', 'title')


class JobDetailView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = JobDetailSerializer
    lookup_field = 'pk'


class JobApplicationListView(ListAPIView):
    queryset = Application.objects.all()
    serializer_class = JobApplicationListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = Application.objects.filter(applicant=self.request.user)

        return queryset


class JobApplicationCreateView(CreateAPIView):
    queryset = Application.objects.all()
    serializer_class = JobApplicationCreateSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        applicant = request.user

        if applicant:
            if serializer.is_valid(raise_exception=True):
                # job_post = Post.objects.get(id=self.kwargs['job_post'])
                # print("JOB ID: %s" % serializer['job_post'])
                serializer.save(applicant=applicant)
                return Response({'detail': _('Your application has been submitted successfully')})
            else:
                return Response({'detail': 'Exception Error'})
        else:
            return Response({'detail': _('You are not authorized')},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


class JobsMatchedView(ListAPIView):
    serializer_class = JobListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        skills = []
        for skill in Skill.objects.filter(candidate__account=self.request.user):
            skills.append(skill.name_id)

        # for in Experience.objects.filter(candidate__account=self.request.user)

        queryset = Post.objects.filter(location=self.request.user.location).filter(skills__in=skills)

        return queryset


class SavedJobsListView(ListAPIView):
    queryset = SavedJob.objects.all()
    serializer_class = SavedJobListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = SavedJob.objects.filter(account=self.request.user)

        return queryset


class SavedJobsCreateView(CreateAPIView):
    queryset = SavedJob.objects.all()
    serializer_class = SavedJobCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        account = request.user

        if account:
            if serializer.is_valid(raise_exception=True):
                serializer.save(account=account)
                return Response({'detail': _('Job has been saved')})
