import django_filters

from job.models import Post


class JobFilter(django_filters.FilterSet):
    class Meta:
        model = Post
        fields = ['category']
