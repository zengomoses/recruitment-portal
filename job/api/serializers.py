from rest_framework import serializers
from rest_framework.relations import StringRelatedField

from accounts.models import User
from company.models import Company
from job.models import Post, Requirement, Benefit, Responsibility, Application, SavedJob


class JobRequirementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requirement
        fields = '__all__'


class JobBenefitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Benefit
        fields = '__all__'


class JobAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class JobResponsibilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Responsibility
        fields = '__all__'


class JobListSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    type = serializers.StringRelatedField(many=True)
    category = serializers.StringRelatedField()
    location = serializers.StringRelatedField(many=True)
    skills = serializers.StringRelatedField(many=True)

    class Meta:
        model = Post
        fields = ['id', 'title', 'author', 'company', 'category', 'type', 'location', 'published_on', 'skills',
                  'min_experience_years', 'max_experience_years',
                  'min_salary', 'max_salary', 'application_deadline', 'application_instructions']


class JobDetailSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    type = serializers.StringRelatedField(many=True)
    category = serializers.StringRelatedField()
    location = serializers.StringRelatedField(many=True)
    job_responsibilities = serializers.StringRelatedField(many=True)
    job_requirements = serializers.StringRelatedField(many=True)
    job_benefits = serializers.StringRelatedField(many=True)
    skills = serializers.StringRelatedField(many=True)

    class Meta:
        model = Post
        fields = ['id', 'title', 'author', 'company', 'category', 'location', 'type', 'published_on', 'description',
                  'job_responsibilities',
                  'job_requirements', 'job_benefits', 'skills', 'min_experience_years', 'max_experience_years',
                  'min_salary', 'max_salary', 'application_deadline', 'application_instructions']


class JobApplicationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['job']


class JobApplicationListSerializer(serializers.ModelSerializer):
    job = JobListSerializer()
    level = serializers.StringRelatedField()

    class Meta:
        model = Application
        fields = ['job', 'level']


class SavedJobCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavedJob
        fields = ['job']


class SavedJobListSerializer(serializers.ModelSerializer):
    job = JobListSerializer()

    class Meta:
        model = SavedJob
        fields = ['job']
