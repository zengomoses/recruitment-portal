import datetime

from django.db import models

# Create your models here.
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone
from tinymce.models import HTMLField

from accounts.models import User
from company.models import Company
from configuration.models import JobCategory, JobType, JobTag, Location, Skill, ApplicationLevel, \
    ApplicationDecisionOption, ApplicationCheck


class Post(models.Model):
    author = models.ForeignKey(Company, on_delete=models.CASCADE)
    title = models.CharField(max_length=64, unique=False)
    company = models.CharField(max_length=100, null=True, blank=True)
    category = models.ForeignKey(JobCategory, related_name="category_post", on_delete=models.CASCADE)
    type = models.ManyToManyField(JobType, related_name='type_post')
    tags = models.ManyToManyField(JobTag)
    description = HTMLField()
    min_salary = models.IntegerField(null=True, blank=True)
    max_salary = models.IntegerField(null=True, blank=True)
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateField(auto_now_add=True)
    published_on = models.DateTimeField(null=True, blank=True)
    valid_until = models.DateField(null=True, blank=True)
    location = models.ManyToManyField(Location, related_name="location_post")
    skills = models.ManyToManyField(Skill, related_name="job_skills")
    min_experience_years = models.IntegerField(default=0)
    max_experience_years = models.IntegerField(default=0)
    application_deadline = models.DateTimeField(null=True, blank=True)
    application_instructions = models.TextField(null=True, blank=True)
    publish = models.BooleanField(default=False)
    force_apply_on_company_website = models.BooleanField(default=False)
    force_apply_in_system = models.BooleanField(default=False)
    force_application_instructions = models.BooleanField(default=False)
    force_application_via_email = models.BooleanField(default=False)
    application_url = models.URLField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    slug = models.SlugField(max_length=500, unique=True, blank=True)

    class Meta:
        get_latest_by = 'published_on'

    def __str__(self):
        return "{}".format(self.title)

    def get_absolute_url(self):
        return reverse('job-detail', args=[str(self.id)])

    def __publish(self):
        if self.publish:
            self.published_on = timezone.now()
            self.valid_until = self.published_on + datetime.timedelta(days=30)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        self.slug = "{}-{}".format(self.id, self.slug)
        if not self.id:
            self.__publish()
        super(Post, self).save(*args, **kwargs)


class Responsibility(models.Model):
    job_post = models.ForeignKey(Post, related_name="job_responsibilities", on_delete=models.CASCADE)
    description = models.TextField()

    class Meta:
        verbose_name_plural = "Responsibilities"

    def __str__(self):
        return "{}".format(self.description)


class Requirement(models.Model):
    job_post = models.ForeignKey(Post, related_name='job_requirements', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.description)


class Benefit(models.Model):
    job_post = models.ForeignKey(Post, related_name='job_benefits', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{}".format(self.description)


class Application(models.Model):
    applicant = models.ForeignKey(User, related_name="job_applicant", on_delete=models.CASCADE)
    job = models.ForeignKey(Post, related_name="job_applications", on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    checklist = models.ManyToManyField(ApplicationCheck)
    level = models.ForeignKey(ApplicationLevel, blank=True, null=True, on_delete=models.SET_NULL)
    decision = models.ForeignKey(ApplicationDecisionOption, blank=True, null=True, on_delete=models.CASCADE)
    decision_made_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        unique_together = ('applicant', 'job')

    def __str__(self):
        return "{}-{}".format(self.applicant, self.job)

    def __set_level(self):
        prev = self.level
        for level in ApplicationLevel.objects.all():
            checks = level.applicationcheck_set.filter(required=True)
            i = 0
            for check in checks:
                if check in self.checklist.all():
                    i += 1
            if not i >= checks.count():
                break
            prev = level
        print("=====ApplicationLevel: %s" % prev)
        self.level = prev


    def save(self, *args, **kwargs):
        if self._state.adding:
            print("STATE ADDING: ", self._state.adding)
        else:
            print("STATE ADDING: ", self._state.adding)
        print("=======Checking if id exists========")
        if self.id:
            print("ID found %s" % self.id)
            print("========Current Application Level : ", self.level)
            print("========Setting Application Level=======")
            self.__set_level()
        else:
            print("ID not found")
            level = ApplicationLevel.objects.get(order=1)
            if level:
                print("Set level to %s" % level)
                self.level = level
        # Create Contact Log entry on application decision
        if self.decision and self.id:
            old = Application.objects.get(id=self.id)
            if not old.decision:
                contact_log = ContactLog(
                    user=self.decision_made_by,
                    application=self,
                    note="Application Decision: %s" % (self.decision,)
                )
                contact_log.save()
        super(Application, self).save(*args, **kwargs)


class SavedJob(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['account', 'job']


class ContactLog(models.Model):
    application = models.ForeignKey(Application, on_delete=models.DO_NOTHING)
    date = models.DateField()
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.DO_NOTHING)
    note = models.TextField()

    def __str__(self):
        return "%s %s: %s" % (self.user, self.date, self.note)

    def save(self, *args, **kwargs):
        if not self.date and not self.id:
            self.date = datetime.date.today()
        super(ContactLog, self).save(*args, **kwargs)
