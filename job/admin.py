from django.contrib import admin

# Register your models here.
from job.models import Post, Requirement, Responsibility, Benefit, Application


class ResponsibilitiesInline(admin.TabularInline):
    model = Responsibility
    extra = 0


class RequirementsInline(admin.TabularInline):
    model = Requirement
    extra = 0


class BenefitsInline(admin.TabularInline):
    model = Benefit
    extra = 0


class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author')
    inlines = [ResponsibilitiesInline, RequirementsInline, BenefitsInline]


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'applicant', 'job', 'date_created', 'decision', 'decision_made_by')


admin.site.register(Post, PostAdmin)
admin.site.register(Application, ApplicationAdmin)