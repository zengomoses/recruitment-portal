from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from configuration.api.serializers import *


class LocationView(ListCreateAPIView):
    serializer_class = LocationSerializer
    queryset = Location.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class JobTypeView(ListCreateAPIView):
    serializer_class = JobTypeSerializer
    queryset = JobType.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class JobCategoryView(ListCreateAPIView):
    serializer_class = JobCategorySerializer
    queryset = JobCategory.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class IndustryView(ListCreateAPIView):
    serializer_class = IndustrySerializer
    queryset = Industry.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class DepartmentView(ListCreateAPIView):
    serializer_class = DepartmentSerializer
    queryset = Department.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class SkillView(ListCreateAPIView):
    serializer_class = SkillSerializer
    queryset = Skill.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class CertificationView(ListCreateAPIView):
    serializer_class = CertificationSerializer
    queryset = Certification.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class InstitutionView(ListCreateAPIView):
    serializer_class = InstitutionSerializer
    queryset = Institution.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class EducationLevelView(ListCreateAPIView):
    serializer_class = EducationLevelSerializer
    queryset = EducationLevel.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class CourseView(ListCreateAPIView):
    serializer_class = CourseSerializer
    queryset = Course.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class JobTagView(ListCreateAPIView):
    serializer_class = JobTagSerializer
    queryset = JobTag.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


class PositionLevelView(ListCreateAPIView):
    serializer_class = PositionLevelSerializer
    queryset = PositionLevel.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]

