from django.urls import path

from configuration.api.views import *

urlpatterns = [
    path('location', LocationView.as_view(), name="location"),
    path('jobtype', JobTypeView.as_view(), name="job_type"),
    path('jobcategory', JobCategoryView.as_view(), name="job_category"),
    path('industry', IndustryView.as_view(), name="industry"),
    path('department', DepartmentView.as_view(), name="department"),
    path('skill', SkillView.as_view(), name="skill"),
    path('certification', CertificationView.as_view(), name="certification"),
    path('institution', InstitutionView.as_view(), name="institution"),
    path('educationlevel', EducationLevelView.as_view(), name="education_level"),
    path('course', CourseView.as_view(), name="course"),
    path('jobtag', JobTagView.as_view(), name="job_tag"),
    path('positionlevel', PositionLevelView.as_view(), name="position_level"),
]