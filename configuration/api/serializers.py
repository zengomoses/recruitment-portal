from rest_framework import serializers

from configuration.models import *


class LocationSerializer(serializers.ModelSerializer):
    jobs = serializers.IntegerField(source='location_post.count', read_only=True)
    class Meta:
        model = Location
        fields = ('id', 'region', 'jobs')


class JobTypeSerializer(serializers.ModelSerializer):
    jobs = serializers.IntegerField(source='type_post.count', read_only=True)
    class Meta:
        model = JobType
        fields = ('id', 'name', 'jobs')


class JobCategorySerializer(serializers.ModelSerializer):
    jobs = serializers.IntegerField(source='category_post.count', read_only=True)
    class Meta:
        model = JobCategory
        fields = ('id', 'name', 'jobs')


class IndustrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Industry
        fields = ('id', 'name')


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name')


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('id', 'name')


class CertificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Certification
        fields = ('id', 'name')


class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institution
        fields = ('id', 'name')


class EducationLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = EducationLevel
        fields = ('id', 'name')


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'name')


class JobTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobTag
        fields = ('id', 'name')


class PositionLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionLevel
        fields = ('id', 'name')
