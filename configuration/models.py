from django.db import models


class Location(models.Model):
    region = models.CharField(max_length=100)

    def __str__(self):
        return self.region


class JobType(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Job Type"

    def __str__(self):
        return self.name


class JobCategory(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Job Category"
        verbose_name_plural = "Job Categories"

    def __str__(self):
        return self.name


class Industry(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Industry"
        verbose_name_plural = "Industries"

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{}".format(self.name)


class Skill(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Certification(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Institution(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class EducationLevel(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Education Level"

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class JobTag(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Job Tag"

    def __str__(self):
        return self.name


class PositionLevel(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Position Level"

    def __str__(self):
        return self.name


class ApplicationLevel(models.Model):
    name = models.CharField(max_length=255, unique=True)
    order = models.IntegerField(unique=True, help_text="Order in which level appears. 1 being first")

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return self.name

    def edit(self):
        return "Edit"

    def show_checks(self):
        """Show checks needed for this level"""
        msg = '|'
        for check in self.applicationcheck_set.all():
            msg += "%s | " % (check.name,)
        return msg


class ApplicationCheck(models.Model):
    name = models.CharField(max_length=255)
    level = models.ForeignKey(ApplicationLevel, on_delete=models.CASCADE)
    required = models.BooleanField(default=True, help_text="When True, applicant cannot meet any level beyond this."
                                                           "When False, applicant can leapfrog check items.")

    class Meta:
        ordering = ('level', 'name',)

    def __str__(self):
        return self.name


class ApplicationDecisionOption(models.Model):
    name = models.CharField(max_length=255, unique=True)
    level = models.ManyToManyField(ApplicationLevel, help_text="This decision can apply for these levels.")

    def __str__(self):
        return self.name
