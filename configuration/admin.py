from django.contrib import admin

# Register your models here.
from configuration.models import Location, JobType, JobCategory, Industry, Department, Skill, Certification, \
    Institution, EducationLevel, Course, JobTag, PositionLevel, ApplicationLevel, ApplicationCheck, \
    ApplicationDecisionOption


class LocationAdmin(admin.ModelAdmin):
    list_display = ['id', 'region']


class JobTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class JobCategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class IndustryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class SkillAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class CertificationAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class InstitutionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class EducationLevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class CourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class JobTagAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class PositionLevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class ApplicationLevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'order']


class ApplicationCheckAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'level', 'required', ]


class ApplicationDecisionOptionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


admin.site.register(Location, LocationAdmin)
admin.site.register(JobType, JobTypeAdmin)
admin.site.register(JobCategory, JobCategoryAdmin)
admin.site.register(Industry, IndustryAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(Certification, CertificationAdmin)
admin.site.register(Institution, InstitutionAdmin)
admin.site.register(EducationLevel, EducationLevelAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(JobTag, JobTagAdmin)
admin.site.register(PositionLevel, PositionLevelAdmin)
admin.site.register(ApplicationLevel, ApplicationLevelAdmin)
admin.site.register(ApplicationCheck, ApplicationCheckAdmin)
admin.site.register(ApplicationDecisionOption, ApplicationDecisionOptionAdmin)
