from django.conf.urls import url
from django.urls import path, include, re_path
from django_filters.views import FilterView
from candidate.filters import jobFilter
from candidate.views.dashboard import DashboardView
from candidate.views.job import SaveJobView, JobListView, JobDetailView, JobsSearchView, ApplyJobView, MyJobsView
from candidate.views.profile import ProfileDetailView, EducationInfo, ExperienceInfo, CertificationInfo, SkillInfo, Cv, \
    EducationDeleteView, ExperienceDeleteView, SkillDeleteView, CertificationDeleteView, CandidateProfileUpdateView, \
    CvDeleteView, CandidateEducationUpdateView, CandidateExperienceUpdateView

urlpatterns = [
    path('', DashboardView.as_view(), name="candidate_dashboard"),
    path('jobs', JobListView.as_view(), name="jobs_list"),
    path('job/save', SaveJobView.as_view(), name="save_job"),
    path('job/myjobs', MyJobsView.as_view(), name="my_jobs"),
    path('job/apply', ApplyJobView.as_view(), name="apply_job"),
    path('job/<int:pk>/detail/', JobDetailView.as_view(), name="candidate_job_detail"),
    path('profile/<int:pk>', ProfileDetailView.as_view(), name="candidate_profile"),
    re_path(r'^jobs/search$', JobsSearchView.as_view(), name="job-candidate-search"),
    # url(r'^jobs/search/$', FilterView.as_view(filterset_class=jobFilter, template_name='candidate/job_list.html'),
    #     name='job-candidate-search'),
    path('profile/education', EducationInfo.as_view(), name="save_education_detail"),
    path('profile/education/delete/<int:pk>', EducationDeleteView.as_view(), name="delete_education_detail"),
    path('profile/experience/delete/<int:pk>', ExperienceDeleteView.as_view(), name="delete_experience_detail"),
    path('profile/certificate/delete/<int:pk>', CertificationDeleteView.as_view(), name="delete_certification_detail"),
    path('profile/skill/delete/<int:pk>', SkillDeleteView.as_view(), name="delete_skill_detail"),
    path('profile/experience', ExperienceInfo.as_view(), name="save_experience_detail"),
    path('profile/certification', CertificationInfo.as_view(), name="save_certification_detail"),
    path('profile/cv', Cv.as_view(), name="save_cv_detail"),
    path('profile/<int:pk>/cv/delete/', CvDeleteView.as_view(), name="delete_cv"),

    path('profile/skill', SkillInfo.as_view(), name="save_skill_detail"),

    path('profile/<int:pk>/update/', CandidateProfileUpdateView.as_view(), name='candidate_profile_update_form'),
    path('profile/education/<int:pk>/update/', CandidateEducationUpdateView.as_view(), name="candidate_education_update_form"),
    path('profile/experience/<int:pk>/update/', CandidateExperienceUpdateView.as_view(), name="candidate_experience_update_form"),


]
