from rest_framework import status, serializers
from rest_framework.generics import CreateAPIView, RetrieveAPIView, GenericAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.utils.translation import ugettext_lazy as _
from rest_framework.views import APIView

from accounts.models import User
from candidate.api.serializers import EducationCreateSerializer, ExperienceCreateSerializer, \
    CertificationCreateSerializer, SkillCreateSerializer, CVFileUploadSerializer, CandidateProfileSerializer
from candidate.models import Candidate, Education, Experience, Certification, Skill


class EducationCreateView(CreateAPIView):
    serializer_class = EducationCreateSerializer
    permission_classes = [IsAuthenticated, ]

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            candidate = Candidate.objects.get(account=request.user)
            if candidate:
                serializer.save(candidate=candidate)
            else:
                candidate = Candidate.objects.create(account=request.user)
                serializer.save(candidate=candidate)
            return Response({'detail': _('Education info saved successfully')}, status=status.HTTP_201_CREATED)


class ExperienceCreateView(CreateAPIView):
    serializer_class = ExperienceCreateSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        candidate = Candidate.objects.get(account=request.user)

        if candidate:
            if serializer.is_valid(raise_exception=True):
                serializer.save(candidate=candidate)
                return Response({'detail': _('Experience info saved successfully')}, status=status.HTTP_201_CREATED)
        return Response({'detail': _('You are not authorized')}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


class CertificationCreateView(CreateAPIView):
    serializer_class = CertificationCreateSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data["certifications"], many=True)
        candidate = Candidate.objects.get(account=request.user)
        if candidate:
            if serializer.is_valid(raise_exception=True):
                serializer.save(candidate=candidate)
                return Response({'detail': _('Certification info saved successfully')}, status=status.HTTP_201_CREATED)
        return Response({'detail': _('You are not authorized')}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


class SkillCreateView(CreateAPIView):
    serializer_class = SkillCreateSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data["skills"], many=True)
        candidate = Candidate.objects.get(account=request.user)

        if candidate:
            if serializer.is_valid(raise_exception=True):
                serializer.save(candidate=candidate)
                return Response({'detail': _('Skills info saved successfully')}, status=status.HTTP_201_CREATED)
        return Response({'detail': _('You are not authorized')}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


class CVFileUploadView(CreateAPIView):
    serializer_class = CVFileUploadSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        candidate = Candidate.objects.get(account=request.user)

        if candidate:
            if serializer.is_valid(raise_exception=True):
                serializer.save(candidate=candidate)
                user = User.objects.get(id=request.user.id)
                user.profile_complete = True
                user.save()
                return Response({'detail': _('CV uploaded successfully')}, status=status.HTTP_201_CREATED)
        return Response({'detail': _('You are not authorized')}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


class CandidateProfileView(ListAPIView):
    serializer_class = CandidateProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = Candidate.objects.filter(account=self.request.user)

        return queryset

