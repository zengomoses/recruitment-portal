from rest_framework import serializers

from candidate.models import Education, Experience, Certification, Skill, CurriculumVitae, Candidate


class EducationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = ('institution', 'start_date', 'end_date', 'education_level', 'course', 'is_highest_level')


class ExperienceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = ('position', 'position_level', 'company', 'department', 'salary_range',
                  'start_date', 'end_date', 'is_current_job')


class CertificationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Certification
        fields = ('name',)


class SkillCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('name',)


class CVFileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = CurriculumVitae
        fields = ('file',)


class EducationListSerializer(serializers.ModelSerializer):
    education_level = serializers.StringRelatedField()
    course = serializers.StringRelatedField()
    class Meta:
        model = Education
        fields = ('institution', 'start_date', 'end_date', 'education_level', 'course', 'is_highest_level')


class ExperienceListSerializer(serializers.ModelSerializer):
    department = serializers.StringRelatedField(read_only=True)
    position_level = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Experience
        fields = ('position', 'position_level', 'company', 'department', 'salary_range',
                  'start_date', 'end_date', 'is_current_job')


class CandidateProfileSerializer(serializers.ModelSerializer):
    candidate_education = EducationListSerializer(many=True, read_only=True)
    candidate_experience = ExperienceListSerializer(many=True, read_only=True)
    candidate_certification = serializers.StringRelatedField(many=True, read_only=True)
    candidate_skill = serializers.StringRelatedField(many=True, read_only=True)
    candidate_cv = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Candidate
        fields = (
            'candidate_education',
            'candidate_experience',
            'candidate_certification',
            'candidate_skill',
            'candidate_cv'
        )
