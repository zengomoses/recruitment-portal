from django.urls import path
from candidate.api.views import EducationCreateView, ExperienceCreateView, CertificationCreateView, SkillCreateView, \
    CVFileUploadView, CandidateProfileView

urlpatterns = [
    path('education', EducationCreateView.as_view(), name="education_info"),
    path('experience', ExperienceCreateView.as_view(), name="experience_info"),
    path('certifications', CertificationCreateView.as_view(), name="certifications_info"),
    path('skills', SkillCreateView.as_view(), name="skills_info"),
    path('cv', CVFileUploadView.as_view(), name="cv_upload"),
    path('profile', CandidateProfileView.as_view())
]