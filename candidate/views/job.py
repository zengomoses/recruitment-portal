import json

from django.contrib import messages
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import CreateView, ListView, DetailView, TemplateView

from backend import settings
from job.models import Post, SavedJob, Application


class ApplyJobView(CreateView):
    model = Application

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            post_instance = Post.objects.get(id=request.POST.get('job'))
            obj = Application(
                applicant=request.user,
                job=post_instance
            )
            application_exists = Application.objects.filter(job=post_instance, applicant=request.user)
            if application_exists:
                response = {'type': 'error', 'message': 'You have already applied for this job post'}
                return JsonResponse(response)
            else:
                if post_instance.force_application_via_email is True:
                    print("natuma email")

                    subject = post_instance.title + " "+" post job application"
                    from_email = settings.EMAIL_HOST_USER
                    to_email = [post_instance.email]
                    application_message = request.POST.get('cover_letter')
                    send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=application_message,
                              fail_silently=False)
                    obj.save()
                    response = {'type': 'success', 'message': 'Your application has been sent successfully.'}
                    return JsonResponse(response)
                else:
                    obj.save()
                    response = {'type': 'success', 'message': 'Your application has been sent successfully.'}
                    return JsonResponse(response)


class SaveJobView(CreateView):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            post_instance = Post.objects.get(id=request.POST.get('job'))
            obj = SavedJob(
                account=request.user,
                job=post_instance
            )
            post_exists = SavedJob.objects.filter(job=post_instance, account=request.user)
            if post_exists:
                response = {'type': 'error', 'message': 'Job has already been saved'}
                return JsonResponse(response)
            else:
                obj.save()
                response = {'type': 'success', 'message': 'Job Post saved has been saved'}
                return JsonResponse(response)


class JobListView(ListView):
    template_name = 'candidate/job_list.html'
    model = Post
    context_object_name = 'jobs'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(JobListView, self).get_context_data(**kwargs)
        context['joblist'] = 'active'

        return context


class JobDetailView(DetailView):
    template_name = 'candidate/job_detail.html'
    model = Post
    context_object_name = 'job'

    def get_context_data(self, **kwargs):
        context = super(JobDetailView, self).get_context_data(**kwargs)
        context['views_count'] = Application.objects.filter(job=Post.objects.get(pk=self.kwargs['pk'])).exclude(
            applicant=self.request.user).count()

        return context


class JobsSearchView(ListView):
    template_name = 'candidate/job_list.html'
    context_object_name = 'jobs'

    def get_queryset(self):
        request = self.request
        query_title = request.GET.get('title')
        query_location = request.GET.get('location')

        if query_title is not None and query_location is not None:
            queryset = Post.objects.filter(title__icontains=query_title, location__region__icontains=query_location)
            print(queryset)
            return queryset

        elif query_title is None and query_location is not None:
            queryset = Post.objects.filter(location__region__icontains=query_location)
            print(queryset)
            return queryset

        elif query_title is not None and query_location is None:
            queryset = Post.objects.filter(title__icontains=query_title)
            print(queryset)
            return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(JobsSearchView, self).get_context_data(**kwargs)
        context['joblist'] = 'active'

        return context


class MyJobsView(TemplateView):
    template_name = 'candidate/my_jobs.html'

    def get_saved_jobs(self):
        queryset = SavedJob.objects.filter(account=self.request.user)

        return queryset

    def get_applied_jobs(self):
        queryset = Application.objects.filter(applicant=self.request.user)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(MyJobsView, self).get_context_data(**kwargs)
        context['myjobs'] = 'active'
        context['applied_jobs'] = self.get_applied_jobs()

        return context
