import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView
from django.shortcuts import render


# Create your views here.
from candidate.models import Skill, ProfileViews
from job.models import Post, SavedJob, Application


class DashboardView(TemplateView):
    template_name = 'candidate/dashboard.html'


    def get_matched_jobs(self):
        skills = []
        for skill in Skill.objects.filter(candidate__account=self.request.user):
            skills.append(skill.name_id)

        queryset = Post.objects.filter(location=self.request.user.location).filter(skills__in=skills).distinct()

        return queryset

    def get_saved_jobs(self):
        queryset = SavedJob.objects.filter(account=self.request.user)

        return queryset

    def get_applied_jobs(self):
        queryset = Application.objects.filter(applicant=self.request.user)

        return queryset

    def get_views_count(self):
        today = datetime.datetime.now()
        views_count = ProfileViews.objects.filter(candidate_id=self.request.user.id, view_date__month=today.month, view_date__year=today.year).count()
        return views_count

    def get_views_details(self):
        today = datetime.datetime.now()
        views_detail = ProfileViews.objects.filter(candidate_id=self.request.user.id, view_date__month=today.month, view_date__year=today.year)

        ProfileViews.objects.filter()
        return views_detail


    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['dashboard'] = 'active'
        context['matched_jobs'] = self.get_matched_jobs()
        context['saved_jobs'] = self.get_saved_jobs()
        context['applied_jobs'] = self.get_applied_jobs()
        context['profile_views'] = self.get_views_count()
        context['viewer'] = self.get_views_details()
        context['latest_jobs'] = Post.objects.filter(published_on__isnull=False).order_by('-published_on')[:6]

        # ProfileViews.objects.filter(recruiter__company_user__name)

        return context



