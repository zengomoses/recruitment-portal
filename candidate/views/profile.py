from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView, TemplateView, CreateView, DeleteView, UpdateView
from django.views.generic.base import View

from accounts.models import User
from candidate.forms import CandidateProfileUpdateForm, CandidateEducationUpdateForm, CandidateExperienceUpdateForm
from candidate.models import Candidate, Education, Experience, Certification as candidater_certification, \
    Skill as candidate_skill, CurriculumVitae, Skill
from company.models import Company
from configuration.models import EducationLevel, Course, PositionLevel, Department, Skill as conf_skill, \
    Certification as config_certification


class ProfileDetailView(DetailView):
    template_name = 'candidate/profile.html'
    model = User
    context_object_name = 'profile'

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        context['myprofile'] = 'active'
        context['candidates'] = Candidate.objects.filter(account=self.request.user)
        context['education_levels'] = EducationLevel.objects.all()
        context['courses'] = Course.objects.all()
        context['position_levels'] = PositionLevel.objects.all()
        context['departments'] = Department.objects.all()
        context['certifications'] = config_certification.objects.all()
        context['skills'] = conf_skill.objects.all()
        context['cv'] = CurriculumVitae.objects.filter(candidate__account=self.request.user).order_by('-id')[:1]
        return context


class EducationInfo(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)

            education = Education()
            education.candidate = Candidate.objects.get(account=request.user)
            education.institution = request.POST.get('institution')
            education.start_date = request.POST.get('start_date')
            education.end_date = request.POST.get('end_date')
            education.education_level = EducationLevel.objects.get(id=request.POST.get('education_level'))
            education.course = Course.objects.get(pk=request.POST.get('course'))
            education.save()

            response = {'type': 'success', 'message': 'Education information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class EducationDeleteView(DeleteView):
    model = Education

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.request.user.id)
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class ExperienceInfo(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)

            experience = Experience()
            experience.candidate = Candidate.objects.get(account=request.user)
            experience.position = request.POST.get('position')
            experience.position_level = PositionLevel.objects.get(id=request.POST.get('position_level'))
            experience.company = request.POST.get('company')
            experience.department = Department.objects.get(id=request.POST.get('department'))
            experience.start_date = request.POST.get('start_date')
            experience.end_date = request.POST.get('end_date')
            experience.save()
            response = {'type': 'success', 'message': 'Experience information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class ExperienceDeleteView(DeleteView):
    model = Experience

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.request.user.id)
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class CertificationInfo(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)
            data = request.POST.getlist('name[]')
            for i in data:
                certification = candidater_certification()
                certification.candidate = Candidate.objects.get(account=request.user)
                certification.name = config_certification.objects.get(id=i)
                certification.save()
                print("Saved Certification {}".format(certification.name))
            response = {'type': 'success', 'message': 'Certification information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class CertificationDeleteView(DeleteView):
    model = candidater_certification

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.request.user.id)
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class SkillInfo(View):
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)
            data = request.POST.getlist('name[]')
            for i in data:
                skill = candidate_skill()
                skill.candidate = Candidate.objects.get(account=request.user)
                skill.name = conf_skill.objects.get(id=i)
                skill.save()
            response = {'type': 'success', 'message': 'Skill information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class SkillDeleteView(DeleteView):
    model = Skill

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.request.user.id)
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class Cv(CreateView):
    model = CurriculumVitae
    fields = ['candidate', 'file']
    template_name = 'candidate/profile.html'

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            cv_instance = CurriculumVitae()
            cv_instance.candidate = Candidate.objects.get(account=request.user)
            cv_instance.file = request.FILES.get('cv')
            print(cv_instance)
            cv_instance.save()
            if cv_instance:
                response = {"type": "success", "message": "Cv saved"}

                messages.success(request, 'Cv Saved Successifully!')
                return HttpResponseRedirect('/jobseeker/profile/' + str(request.user.id))


class CvDeleteView(DeleteView):
    model = CurriculumVitae

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.request.user.id)
        return success_url

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class CandidateProfileUpdateView(UpdateView):
    model = User
    context_object_name = 'profile'
    form_class = CandidateProfileUpdateForm
    template_name = 'candidate/profile_update.html'

    def get_object(self, queryset=None):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(User, id=id_)

    def get_success_url(self):
        success_url = '/jobseeker/profile/' + str(self.kwargs.get('pk'))
        return success_url


class CandidateEducationUpdateView(UpdateView):
    template_name = 'candidate/education_update_form.html'
    model = Education
    context_object_name = 'edu'
    form_class = CandidateEducationUpdateForm

    def get_object(self, queryset=None):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(Education, id=id_)

    def get_success_url(self):
        print("imeingia")
        success_url = '/jobseeker/profile/' + str(self.kwargs.get('pk'))
        return success_url


class CandidateExperienceUpdateView(UpdateView):
    template_name = 'candidate/experience_update_form.html'
    model = Experience
    context_object_name = 'comp'
    form_class = CandidateExperienceUpdateForm

    def get_object(self, queryset=None):
        id_ = self.kwargs.get("pk")
        print(get_object_or_404(Experience, id=id_))
        return get_object_or_404(Experience, id=id_)

    def get_success_url(self):
        print("imeingia")
        success_url = '/jobseeker/profile/' + str(self.kwargs.get('pk'))
        return success_url


# class CandidateSkillUpdateView(UpdateView):
#     template_name = 'candidate/skill_update_form.html'
#     model = candidate_skill
#     context_object_name = 'skill'
#     form_class = CandidateSkillUpdateForm
#
#     def get_object(self, queryset=None):
#         id_ = self.kwargs.get("pk")
#         print(get_object_or_404(candidate_skill, id=id_))
#         return get_object_or_404(candidate_skill, id=id_)
#
#     def get_success_url(self):
#         print("imeingia")
#         success_url = '/jobseeker/profile/' + str(self.kwargs.get('pk'))
#         return success_url
