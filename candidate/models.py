import os
import random
import string
from uuid import uuid4

from django.core.exceptions import ValidationError
from django.db import models

from accounts.models import User
from company.models import Company
from configuration.models import Institution, EducationLevel, Course, PositionLevel, Department, Certification, Skill


class Candidate(models.Model):
    # PERMANENT = 'Permanent'
    # CURRENT = 'Current'
    # ADDRESS_TYPE = (
    #     (PERMANENT, 'Permanent Address'),
    #     (CURRENT, 'Current Address'),
    # )

    account = models.OneToOneField(User, related_name='candidate_user', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Profile"

    def __str__(self):
        return '{} {}'.format(self.account.first_name, self.account.last_name)


class Education(models.Model):
    candidate = models.ForeignKey(Candidate, related_name="candidate_education", on_delete=models.DO_NOTHING)
    institution = models.CharField(max_length=225)
    # institution = models.ForeignKey(Institution, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    education_level = models.ForeignKey(EducationLevel, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    is_highest_level = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Education Information"
        verbose_name_plural = "Education Information"

    def __str__(self):
        return '{} ({})'.format(self.course, self.education_level)


class ProfileViews(models.Model):
    candidate = models.ForeignKey(User, related_name="candidate_user_profileView", on_delete=models.DO_NOTHING)
    recruiter = models.ForeignKey(User, related_name="recruiter_user", on_delete=models.DO_NOTHING)
    company = models.ForeignKey(Company, related_name="recruiter_company", on_delete=models.DO_NOTHING)
    view_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Profile View"
        verbose_name_plural = "Profile View"
        unique_together = ['candidate', 'recruiter']


    def __str__(self):
        return '{}-{}'.format(self.recruiter, self.candidate)

    def get_recruiter_company(self):
        company = Company.objects.get(account_id=self.recruiter.id)

        return company


class Experience(models.Model):
    candidate = models.ForeignKey(Candidate, related_name="candidate_experience", on_delete=models.CASCADE)
    position = models.CharField(max_length=100)
    position_level = models.ForeignKey(PositionLevel, on_delete=models.CASCADE)
    company = models.CharField(max_length=100)
    department = models.ForeignKey(Department, on_delete=models.CASCADE),
    salary_range = models.CharField(max_length=100, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    is_current_job = models.BooleanField(default=False, null=True, blank=True)

    class Meta:
        verbose_name = "Work Experience"
        ordering = ['-start_date']

    def __str__(self):
        return '{}-{}'.format(self.position, self.company)


class Certification(models.Model):
    candidate = models.ForeignKey(Candidate, related_name="candidate_certification", on_delete=models.CASCADE)
    name = models.ForeignKey(Certification, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Professional Certification"
        unique_together = ['candidate', 'name']

    def __str__(self):
        return "{}".format(self.name)


class Skill(models.Model):
    candidate = models.ForeignKey(Candidate, related_name="candidate_skill", on_delete=models.CASCADE)
    name = models.ForeignKey(Skill, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['candidate', 'name']

    def __str__(self):
        return "{}".format(self.name)


def content_file_name(instance, filename):
    ext = filename.split('.')[-1]
    f_name = '-'.join(filename.replace('.pdf', '').split())
    # rand_strings = ''.join(random.choice(string.lowercase + string.digits) for i in range(10))
    filename = '{}_{}.{}'.format(f_name, uuid4().hex, ext)
    return os.path.join('files/pdf/cv/', filename)


class CurriculumVitae(models.Model):
    candidate = models.ForeignKey(Candidate, related_name="candidate_cv", on_delete=models.DO_NOTHING)
    file = models.FileField(upload_to=content_file_name, blank=False, null=False)

    class Meta:
        verbose_name = "Curriculum Vitae"
        unique_together = ['candidate', 'file']

    def __str__(self):
        return "{}".format(self.file)
