from django import forms

from accounts.models import User
from candidate.models import Education, Experience, Skill


class PersonalInfoForm(forms.Form):
    first_name = forms.CharField(max_length=20)
    last_name = forms.CharField(max_length=20)
    title = forms.CharField(max_length=20)
    gender = forms.CharField(max_length=20)
    date_of_birth = forms.DateField()
    phone_number = forms.CharField(max_length=20)
    location = forms.CharField(max_length=20)
    pic = forms.ImageField(max_length=200)


class ProfileInfoForm(forms.Form):
    category = forms.CharField(max_length=20)


class CandidateProfileUpdateForm(forms.ModelForm):
    first_name = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'name': 'first_name',
                                      'placeholder': 'First name'}),
    )
    last_name = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'name': 'last_name',
                                      'placeholder': 'Last name'}),
    )
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'name': 'username', 'placeholder': 'Username'}),
    )
    email = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'name': 'email', 'placeholder': 'Email',
                   'type': 'email'}),
    )

    gender = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
    )

    date_of_birth = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'type': 'date', 'name': 'date_of_birth',
                   'placeholder': 'Birthdate'}),
    )
    phone_number = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'name': 'phone',
                                      'placeholder': 'Phone number'}),
    )
    location = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-md-12'},
    )
    avatar = forms.ImageField(max_length=200)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'gender', 'date_of_birth', 'phone_number', 'location',
                  'avatar', 'username', 'email']


class CandidateEducationUpdateForm(forms.ModelForm):
    institution = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'name': 'Institution',
                                      'placeholder': 'Institution'}),
    )

    course = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
    )

    education_level = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
    )

    start_date = forms.CharField(
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'type': 'date', 'name': 'start_date',
                   'placeholder': 'Start date'}),
    )

    end_date = forms.CharField(
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'type': 'date', 'name': 'end_date',
                   'placeholder': 'End data'}),
    )

    class Meta:
        model = Education
        fields = ['id', 'institution', 'start_date', 'end_date', 'course', 'education_level']


class CandidateExperienceUpdateForm(forms.ModelForm):
    company = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'name': 'company', 'placeholder': 'Company'}),
    )

    position = forms.CharField(
        max_length=254,
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'name': 'position', 'placeholder': 'Position'}),
    )

    is_current_job = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
    )

    position_level = forms.Widget(
        attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
    )

    start_date = forms.CharField(
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'type': 'date', 'name': 'start_date',
                   'placeholder': 'Start date'}),
    )

    end_date = forms.CharField(
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control required', 'type': 'date', 'name': 'end_date',
                   'placeholder': 'End data'}),
    )

    class Meta:
        model = Experience
        fields = ['id', 'company', 'position', 'position_level', 'start_date', 'end_date',
                  'is_current_job']


# class CandidateSkillUpdateForm(forms.ModelForm):
#     name = forms.Widget(
#         attrs={'autofocus': True, 'class': 'form-control col-sm-12'},
#     )
#
#     class Meta:
#         model = Skill
#         fields = ['id', 'name']
