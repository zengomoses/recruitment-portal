import django_filters
from job.models import Post


class jobFilter(django_filters.FilterSet):
    class Meta:
        model = Post
        fields = ['location', 'title']

