from django.contrib import admin

# Register your models here.
from candidate.models import Candidate, Education, Certification, Experience, CurriculumVitae, Skill, ProfileViews


class EducationInline(admin.TabularInline):
    model = Education
    extra = 0


class ExperienceInline(admin.TabularInline):
    model = Experience
    extra = 0


class ProfileViewAdmin(admin.ModelAdmin):
     list_display = ('candidate', 'recruiter', 'view_date', 'company')


class CertificationInline(admin.TabularInline):
    model = Certification
    extra = 0


class SkillInline(admin.TabularInline):
    model = Skill
    extra = 0


class CurriculumVitaeInline(admin.TabularInline):
    model = CurriculumVitae
    extra = 0


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('account',)
    inlines = [EducationInline, ExperienceInline, CertificationInline, SkillInline, CurriculumVitaeInline]


admin.site.register(Candidate, CandidateAdmin)
admin.site.register(ProfileViews, ProfileViewAdmin)
