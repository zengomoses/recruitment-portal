from django.conf.urls import url
from django.urls import path
from requests import request

import recruiter
from recruiter.filters import UserFilter
from recruiter.views.candidate_profile import CandidateProfileDetailView, ApplicantsPerPost, RecruiterDecision, \
    CandidateProfilePerPostDetailView, RecruiterDecisionAction
from recruiter.views.views import DashboardView, JobListView, JobPostCreate
from django_filters.views import FilterView

urlpatterns = [
    path('', DashboardView.as_view(), name="recruiter_dashboard"),
    path('job/posts', JobListView.as_view(), name="recruiter_job_list"),
    url(r'^candidates/search/$', FilterView.as_view(filterset_class=UserFilter,
                                                    template_name='recruiter/applicants_list.html'), name='applications-search'),
    path('job-posts/create', JobPostCreate.as_view(), name="save_jobpost_detail"),
    path('job/<int:pk>/applications/', ApplicantsPerPost.as_view(), name="applicants_per_post"),
    path('candidate-profile/<int:pk>', CandidateProfileDetailView.as_view(), name="candidate_profile_view"),
    path('job/applications/profile/<int:application_id>/<int:pk>', CandidateProfilePerPostDetailView.as_view(), name="candidate_application_profile_view"),
    path('candidate-view', RecruiterDecision.as_view(), name="candidate_decision"),
    path('candidate-decision', RecruiterDecisionAction.as_view(), name="candidate_decision_action"),

]

# handler404 = error_404(request)