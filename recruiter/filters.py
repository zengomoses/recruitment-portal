import django_filters
from candidate.models import Candidate


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = Candidate
        fields = ['account__location', 'candidate_education__education_level', 'candidate_skill__name', 'candidate_certification__name', 'candidate_education__course']

