from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, TemplateView, CreateView, ListView, UpdateView
from django.views.generic.base import View

from accounts.models import User
from candidate.forms import CandidateProfileUpdateForm
from candidate.models import Candidate, Education, Experience, Certification as candidater_certification, \
    Skill as candidate_skill, CurriculumVitae, ProfileViews
from company.models import Company
from job.models import Application, Post
from configuration.models import EducationLevel, Course, PositionLevel, Department, Skill as conf_skill, \
    Certification as config_certification, ApplicationDecisionOption, ApplicationCheck, ApplicationLevel


class CandidateProfileDetailView(DetailView):
    template_name = 'recruiter/candidate_profile.html'
    model = User
    context_object_name = 'profile'

    def get_context_data(self, **kwargs):
        context = super(CandidateProfileDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_recruiter():
            c = Company.objects.filter(account_id=self.request.user.id)
            if c:
                obj, created = ProfileViews.objects.update_or_create(
                    recruiter=User.objects.get(id=self.request.user.id),
                    candidate=User.objects.get(id=self.kwargs['pk']),
                    company=Company.objects.get(
                        account_id=self.request.user.id))
            # else:

        context['myprofile'] = 'active'
        context['candidates'] = Candidate.objects.filter(account=self.request.user)
        context['education_levels'] = EducationLevel.objects.all()
        context['courses'] = Course.objects.all()
        context['position_levels'] = PositionLevel.objects.all()
        context['departments'] = Department.objects.all()
        context['certifications'] = config_certification.objects.all()
        context['skills'] = conf_skill.objects.all()
        context['cv'] = CurriculumVitae.objects.filter(candidate__account=self.request.user).order_by('-id')[:1]

        context['check'] = ApplicationCheck.objects.all()

        return context


class CandidateProfilePerPostDetailView(DetailView):
    template_name = 'recruiter/candidate_profile_decision.html'
    model = User
    context_object_name = 'profile'

    def get_context_data(self, **kwargs):
        context = super(CandidateProfilePerPostDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_recruiter():
            obj, created = ProfileViews.objects.update_or_create(recruiter=User.objects.get(id=self.request.user.id),
                                                                 candidate=User.objects.get(id=self.kwargs['pk']),
                                                                 company=Company.objects.get(
                                                                     account_id=self.request.user.id))

        context['myprofile'] = 'active'
        context['candidates'] = Candidate.objects.filter(account=self.request.user)
        context['education_levels'] = EducationLevel.objects.all()
        context['courses'] = Course.objects.all()
        context['position_levels'] = PositionLevel.objects.all()
        context['departments'] = Department.objects.all()
        context['certifications'] = config_certification.objects.all()
        context['skills'] = conf_skill.objects.all()
        context['cv'] = CurriculumVitae.objects.filter(candidate__account=self.request.user).order_by('-id')[:1]

        context['application_id'] = self.kwargs['application_id']
        applicant_level = Application.objects.filter(id=self.kwargs['application_id'])
        for o in applicant_level:
            context['candidate_level'] = o.level

            if o.level.order == 2:
                context['decisiom'] = ApplicationDecisionOption.objects.filter(
                    name__in=['Shortlist', 'Accept', 'Reject'])
            elif o.level.order == 3:
                context['decisiom'] = ApplicationDecisionOption.objects.filter(
                    name__in=['Accept', 'Reject'])
            elif o.level.order == 4:
                context['decisiom'] = ApplicationDecisionOption.objects.filter(
                    name__in=['Reject'])

        return context


class ApplicantsPerPost(DetailView):
    template_name = 'recruiter/job_view.html'
    model = Post
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        context = super(ApplicantsPerPost, self).get_context_data(**kwargs)

        # context['decision'] = ApplicationDecisionOption.objects.all()
        # context['check'] = ApplicationCheck.objects.all()

        # print(context['desc'])
        return context


class RecruiterDecision(View):
    # triggered automatically on profile page check per job applied
    def post(self, request, *args, **kwargs):
        application_id = request.POST.get('application_id')
        print("Haya sasa mwenye application num: ", application_id)
        # check_idd = request.POST.get('check_id')
        obj = Application.objects.filter(id=application_id)
        for o in obj:
            print("Kwa sasa yuko level: ", o.level)
            if o.level.order == 1:
                level = ApplicationLevel.objects.get(order=o.level.order + 1)
                print("sasa tunabadilisha level:", o.level)
                # obj.decision_made_by = request.user
                obj.update(level=level)
        # obj.checklist.add(ApplicationCheck.objects.get(id=check_idd))

        response = {'type': 'success', 'message': 'Application decision has been updated'}
        #     else:
        #         response = {'type': 'error', 'message': 'This education information already exists'}
        # else:
        #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
        return JsonResponse(response)


class RecruiterDecisionAction(View):
    # these are triggered by the recruiter in candidate's profile

    def post(self, request, *args, **kwargs):
        application_id = request.POST.get('application_id')
        decision_id = request.POST.get('decision_id')
        level = ''
        # User.objects.filter()
        print("Haya sasa mwenye application num: ", application_id)
        # check_idd = request.POST.get('check_id')
        obj = Application.objects.filter(id=application_id)
        desicion_obj = ApplicationDecisionOption.objects.get(id=decision_id)
        for o in obj:
            print("Kwa sasa yuko level: ", o.level)
            if o.level.order == 2 and desicion_obj.name == "Shortlist":
                level = ApplicationLevel.objects.get(order=o.level.order + 1)
                print("sasa tunabadilisha level:", o.level)
                obj.decision_made_by = request.user
                obj.decision = desicion_obj
                obj.update(level=level, decision_made_by=request.user, decision=desicion_obj)
                response = {'type': 'success', 'message': 'Decision has been succesifully made'}


            elif o.level.order == 3 and desicion_obj.name == "Accept":
                level = ApplicationLevel.objects.get(order=o.level.order + 1)
                print("sasa tunabadilisha level:", o.level)
                obj.decision_made_by = request.user
                obj.update(level=level)
                response = {'type': 'success', 'message': 'Decision has been succesifully made'}

            elif o.level.order == 2 and desicion_obj.name == "Reject":
                level = ApplicationLevel.objects.get(order=o.level.order + 3)
                print("sasa tunabadilisha level:", o.level)
                obj.decision_made_by = request.user
                obj.update(level=level)
                response = {'type': 'success', 'message': 'Decision has been succesifully made'}

            elif o.level.order == 3 and desicion_obj.name == "Reject":
                level = ApplicationLevel.objects.get(order=o.level.order + 2)
                print("sasa tunabadilisha level:", o.level)
                obj.decision_made_by = request.user
                obj.update(level=level)
                response = {'type': 'success', 'message': 'Decision has been succesifully made'}

            elif o.level.order == 4 and desicion_obj.name == "Reject":
                level = ApplicationLevel.objects.get(order=o.level.order + 1)
                print("sasa tunabadilisha level:", o.level)
                obj.decision_made_by = request.user
                obj.update(level=level)
                response = {'type': 'success', 'message': 'Decision has been succesifully made'}

            else:
                response = {'type': 'warning', 'message': 'Application Level did not match Decision'}

            return JsonResponse(response)


class EducationInfo(View):
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)
            education = Education()
            education.candidate = Candidate.objects.get(account=request.user)
            education.institution = request.POST.get('institution')
            education.start_date = request.POST.get('start_date')
            education.end_date = request.POST.get('end_date')
            education.education_level = EducationLevel.objects.get(id=request.POST.get('education_level'))
            education.course = Course.objects.get(pk=request.POST.get('course'))
            education.save()

            response = {'type': 'success', 'message': 'Education information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class ExperienceInfo(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)

            experience = Experience()
            experience.candidate = Candidate.objects.get(account=request.user)
            experience.position = request.POST.get('position')
            experience.position_level = PositionLevel.objects.get(id=request.POST.get('position_level'))
            experience.company = request.POST.get('company')
            experience.department = Department.objects.get(id=request.POST.get('department'))
            experience.start_date = request.POST.get('start_date')
            experience.end_date = request.POST.get('end_date')
            experience.save()
            response = {'type': 'success', 'message': 'Experience information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class CertificationInfo(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)
            data = request.POST.getlist('name[]')
            for i in data:
                certification = candidater_certification()
                certification.candidate = Candidate.objects.get(account=request.user)
                certification.name = config_certification.objects.get(id=i)
                certification.save()
                print("Saved Certification {}".format(certification.name))
            response = {'type': 'success', 'message': 'Certification information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class SkillInfo(View):
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            Candidate.objects.get_or_create(account=request.user)
            data = request.POST.getlist('name[]')
            for i in data:
                skill = candidate_skill()
                skill.candidate = Candidate.objects.get(account=request.user)
                skill.name = conf_skill.objects.get(id=i)
                skill.save()
            response = {'type': 'success', 'message': 'Skill information has been saved successfully'}
            #     else:
            #         response = {'type': 'error', 'message': 'This education information already exists'}
            # else:
            #     response = {'type': 'error', 'message': 'Your account is not registered as a candidate'}
            return JsonResponse(response)


class Cv(CreateView):
    model = CurriculumVitae
    fields = ['candidate', 'file']
    template_name = 'recruiter/candidate_profile.html'

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            cv_instance = CurriculumVitae()
            cv_instance.candidate_id = request.user.id
            cv_instance.file = request.FILES.get('cv')
            print(cv_instance)
            cv_instance.save()
            if cv_instance:
                response = {"type": "success", "message": "Cv saved"}

                messages.success(request, 'Cv Saved Successifully!')
                return HttpResponseRedirect('/jobseeker/profile/' + str(request.user.id))


