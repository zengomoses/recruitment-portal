from datetime import timedelta
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.dateformat import DateFormat
from django.views.generic import TemplateView, ListView
from django.views.generic.base import View

from candidate.models import Candidate, CurriculumVitae
from configuration.models import EducationLevel, Course, PositionLevel, Department, Skill as conf_skill, \
    Certification as config_certification
from job.models import *


class DashboardView(TemplateView):
    template_name = 'recruiter/dashboard.html'

    def get_submitted_applications_count(self):
        if self.request.user.is_recruiter():
            queryset = Application.objects.filter(level__name__iexact='Submitted',
                                                  job__author=Company.objects.get(account=self.request.user)).count()
            return queryset

    def get_shortlisted_count(self):
        if self.request.user.is_recruiter():
            queryset = Application.objects.filter(level__name__iexact='Shortlisted',
                                                  job__author=Company.objects.get(account=self.request.user)).count()
            return queryset

    def get_in_reviews_count(self):
        if self.request.user.is_recruiter():
            queryset = Application.objects.filter(level__name__iexact='In Review',
                                                  job__author=Company.objects.get(account=self.request.user)).count()
            return queryset

    def get_interviewed_count(self):
        if self.request.user.is_recruiter():
            queryset = Application.objects.filter(level__name__iexact='Interviewed',
                                                  job__author=Company.objects.get(account=self.request.user)).count()
            return queryset

    def get_selected_count(self):
        if self.request.user.is_recruiter():
            queryset = Application.objects.filter(level__name__iexact='Selected',
                                                  job__author=Company.objects.get(account=self.request.user)).count()
            return queryset

    def get_active_jobs_posts_count(self):
        if self.request.user.is_recruiter():
            start_date = datetime.date.today()
            end_date = datetime.date.today() + timedelta(days=30)
            print("START DATE: " + str(start_date))
            print("END DATE: " + str(end_date))
            queryset = Post.objects.filter(valid_until__range=[start_date, end_date],
                                           author__account=self.request.user).count()

            return queryset

    def get_active_jobs_posts(self):
        if self.request.user.is_recruiter():
            start_date = datetime.date.today()
            end_date = datetime.date.today() + timedelta(days=30)
            print("START DATE: " + str(start_date))
            print("END DATE: " + str(end_date))
            queryset = Post.objects.filter(valid_until__range=[start_date, end_date],
                                           author__account=self.request.user).order_by('application_deadline')[:5]

            return queryset

    def get_context_data(self, **kwargs):
        if self.request.user.is_recruiter():
            context = super(DashboardView, self).get_context_data(**kwargs)
            context['dashboard'] = 'active'
            context['submitted'] = self.get_submitted_applications_count()
            context['shortlisted'] = self.get_shortlisted_count()
            context['in_review'] = self.get_in_reviews_count()
            context['interviewed'] = self.get_interviewed_count()
            context['selected'] = self.get_selected_count()
            context['active_jobs'] = self.get_active_jobs_posts_count()
            context['top_5_active_jobs'] = self.get_active_jobs_posts()

            return context


class JobListView(ListView):
    template_name = 'recruiter/job_list.html'
    model = Post
    # paginate_by = 3
    context_object_name = 'posts'

    def get_queryset(self):
        queryset = Post.objects.filter(author__account=self.request.user.id)
        print(self.request.user.id)
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(JobListView, self).get_context_data(**kwargs)
        context['job_list'] = 'active'
        context['job_type'] = JobType.objects.all()
        context['job_category'] = JobCategory.objects.all()
        context['job_course'] = Course.objects.all()
        context['job_location'] = Location.objects.all()
        context['job_skills'] = Skill.objects.all()
        context['job_certification'] = config_certification.objects.all()
        context['company'] = Company.objects.all()
        return context


class JobPostCreate(View):

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':

            jobpost = Post()
            jobpost.author = Company.objects.get(account=request.user)
            jobpost.title = request.POST.get('title')
            jobpost.min_experience_years = request.POST.get('min-experience')
            jobpost.description = request.POST.get('description')

            if request.POST.get('min-salary') == "":
                jobpost.min_salary = 0
            else:
                jobpost.min_salary = request.POST.get('min-salary')

            if request.POST.get('max-salary') == "":
                jobpost.max_salary = 0
            else:
                jobpost.max_salary = request.POST.get('max-salary')

            jobpost.company = request.POST.get('company')
            jobpost.application_deadline = request.POST.get('application_deadline')
            jobpost.category = JobCategory.objects.get(pk=request.POST.get('category'))
            jobpost.publish = True
            jobpost.force_application_instructions = True if request.POST.get('force_application_instructions') else False
            jobpost.force_apply_on_company_website = True if request.POST.get('company_website') else False
            jobpost.force_application_via_email = True if request.POST.get('force_application_via_email') else False
            jobpost.force_apply_in_system = True if request.POST.get('apply_in_system') else False
            jobpost.application_url = request.POST.get('application_url')
            jobpost.application_instructions = request.POST.get('application_instructions')
            jobpost.email = request.POST.get('company_email')
            jobpost.save()

            location = request.POST.getlist('location[]')
            for i in location:
                jobpost.location.add(Location.objects.get(id=i))

            type = request.POST.getlist('type[]')
            print(type)
            for i in type:
                jobpost.type.add(JobType.objects.get(pk=i))

            skill = request.POST.getlist('skill[]')
            print(skill)
            for i in skill:
                jobpost.skills.add(Skill.objects.get(pk=i))

            responsibility = request.POST.getlist('responsibilities')
            insert_list = []
            for s in responsibility:
                description = s
                insert_list.append(Responsibility(description=description, job_post=Post.objects.get(id=jobpost.id)))
            Responsibility.objects.bulk_create(insert_list)

            requirements = request.POST.getlist('requirements')
            insert_list = []
            for s in requirements:
                description = s
                insert_list.append(Requirement(description=description, job_post=Post.objects.get(id=jobpost.id)))
            Requirement.objects.bulk_create(insert_list)

            response = {'type': 'success', 'message': 'Job Post has been saved successfully'}

            return JsonResponse(response)


# def error_404(request):
#         data = {}
#         return render(request, '404.html', data)
#
