"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

from backend import settings

schema_view = get_swagger_view(title="CAREER CONNECT API")
urlpatterns = [
    path('swagger', schema_view),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/v1/', include('api.urls')),
    path('', include('main.urls')),
    path('jobseeker/', include('candidate.urls')),
    path('recruiter/', include('recruiter.urls')),
    path('redirecting/', include('redirection.urls')),
    path('account/', include('accounts.urls')),
    path('tinymce/', include('tinymce.urls')),
    url(r'^tracking/', include('tracking.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "CAREER CONNECT"
admin.site.site_title = "CAREER CONNECT"
admin.site.index_title = "Welcome to CAREER CONNECT Recruitment Portal"
